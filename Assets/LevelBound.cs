using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBound : MonoBehaviour
{

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Fighter.instance.transform.position;
    }
  
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.GetComponent<AstroidPieces>() != null)
        {
            Destroy(collision.gameObject);
        }
        if (collision.collider.GetComponent<Bullet>() != null)
        {
            Destroy(collision.gameObject);
        }
    }
}
