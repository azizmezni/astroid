using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Analytics : MonoBehaviour
{
    public static Analytics instance;   
    public int playerDeath;
    public int AstroidKilled;
    public int AstroidSpawned;
    public int bulletShot;
    public float AliveTime;
    private void OnDestroy()
    {
        EventManager.OnPlayerDeath -= OnPlayerDeath;
        EventManager.OnBulletShot -= OnBulletShot;
        EventManager.OnCollisionAstroBullet -= OnCollisionAstroBullet;
        EventManager.OnAstroSpawned -= OnAstroSpawned;
    }

  

    void Start()
    {
        instance = this;
        EventManager.OnPlayerDeath += OnPlayerDeath;
        EventManager.OnBulletShot += OnBulletShot;
        EventManager.OnCollisionAstroBullet += OnCollisionAstroBullet;
        EventManager.OnAstroSpawned += OnAstroSpawned;
    }

    private void OnAstroSpawned(AstroidPieces obj)
    {
        AstroidSpawned++;
    }

    private void OnCollisionAstroBullet(Bullet arg1, AstroidPieces arg2)
    {
        AstroidKilled++;
    }

    private void OnPlayerDeath(Fighter Fighter)
    {
        playerDeath++;
        AliveTime = Fighter.AliveTime;
    }
    private void OnBulletShot(Bullet obj)
    {
        bulletShot++;
    }

}
