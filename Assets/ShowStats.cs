using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using System;

public class ShowStats : MonoBehaviour
{
    public TextMeshProUGUI AstroDestroyed;
    public TextMeshProUGUI BulletShot;
    public TextMeshProUGUI AliveTime;
    public GameObject Holder;
    // Start is called before the first frame update
    void Start()
    {
        EventManager.OnPlayerDeath += OnplayerDeath;
    }
    private void OnDestroy()
    {
        EventManager.OnPlayerDeath -= OnplayerDeath;
    }

    private void OnplayerDeath(Fighter obj)
    {
        Show();
    }

    private void Show()
    {
        Holder.SetActive(true);
        AstroDestroyed.text = Analytics.instance.AstroidKilled.ToString();
        BulletShot.text = Analytics.instance.AstroidKilled.ToString();
        AliveTime.text = Analytics.instance.AliveTime.ToString();
    }
    public void Restart()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(0);

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
