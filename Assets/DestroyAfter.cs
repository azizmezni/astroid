using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfter : MonoBehaviour
{
    public float AliveDuration=10f;
    void Start()
    {
        Destroy(gameObject,AliveDuration);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
