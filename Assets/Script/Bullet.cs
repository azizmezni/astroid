using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Vector2 direction;
    public float bulletSpeed = 30f;
    public Rigidbody2D rigidbody;

    // Start is called before the first frame update
    private void Start()
    {
       
        Invoke(nameof(KillBullet), 50f);
        EventManager.BulletShot(this);
    }

    public void Setup(Vector3 direction)
    {
        rigidbody.AddForce(direction * bulletSpeed);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        KillBullet();
    }
    private void KillBullet()
    {
        Destroy(gameObject);
    }

    // Update is called once per frame
}