using UnityEngine;

public class Fighter : MonoBehaviour
{
    public static Fighter instance;
    private Rigidbody2D rigidbody;
    public Bullet bulletPrefab;

    public float PowerSpeed = 1f;
    private bool Power;

    private float turnDirection = 0f;
    public float rotationSpeed = 0.1f;
    public Transform muzzle;
    public float AliveTime;

    private void Awake()
    {
        instance = this;
        rigidbody = GetComponent<Rigidbody2D>();
        
    }

    private void Update()
    {
        AliveTime += Time.deltaTime;
        Power = Input.GetKey(KeyCode.UpArrow);

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            turnDirection = 1f;
        }
        else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            turnDirection = -1f;
        }
        else
        {
            turnDirection = 0f;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Shoot();
        }
    }

    private void FixedUpdate()
    {
        if (Power)
        {
            rigidbody.AddForce(transform.up * PowerSpeed);
        }

        if (turnDirection != 0f)
        {
            rigidbody.AddTorque(rotationSpeed * turnDirection);
        }
    }

    private void Shoot()
    {
        var bullet = Instantiate(bulletPrefab, muzzle.transform.position, muzzle.transform.rotation);

        bullet.Setup(transform.up);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        EventManager.PlayerDeath(this);
    }
}