using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AstroidManager : MonoBehaviour
{
    public static AstroidManager instance;
  public List<Sprite> sprites = new List<Sprite>();
    public AstroidPieces pieces;
    private List<AstroidPieces> astroidPieces=new List<AstroidPieces>();
      public ParticleSystem DestractionVFX;

    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        AutoGenerateRandomAstroid();
        EventManager.OnAstroKilled += OnAstroKilled;
    
    }
    private void OnDestroy()
    {
        EventManager.OnAstroKilled -= OnAstroKilled;
    }
    private void OnAstroKilled(AstroidPieces astroidPieces)
    {
      
        Gernerate1Astro();
    }

    void AutoGenerateRandomAstroid()
    {
        for (int i = 0; i < 5; i++)
        {
            Gernerate1Astro();
        }

    }

    public void Gernerate1Astro()
    {
        var piece = Instantiate(pieces);

        piece.Gerenate(sprites[Random.Range(0, sprites.Count)]);

    }

   
}
