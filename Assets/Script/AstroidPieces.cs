using UnityEngine;

public class AstroidPieces : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public Rigidbody2D rd;

    // Start is called before the first frame update

    public void Gerenate(Sprite sp)
    {
        spriteRenderer.sprite = sp;
        gameObject.AddComponent<PolygonCollider2D>();
        transform.position = (Vector2)Fighter.instance.transform.position + Random.insideUnitCircle * 10;
        transform.rotation = Quaternion.Euler(0, 0, Random.value * 360);
        rd.AddForce((Fighter.instance.transform.position - transform.position).normalized * 10f);
        rd.AddTorque(Random.value * 10f);
        EventManager.AstroSpawned(this);
    }

    private void kill()
    {
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        EventManager.AstroKilled(this);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.GetComponent<Bullet>())
        {
            EventManager.CollisionAstroBullet(collision.collider.GetComponent<Bullet>(), this);
            kill();
        }
        if (collision.collider.GetComponent<Fighter>())
        {
            EventManager.PlayerDeath(collision.collider.GetComponent<Fighter>());
        }
    }
}