using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
   public static GameManager instance;
    void Start()
    {
        instance = this;
        EventManager.OnPlayerDeath += OnplayerDeath;
    }
    private void OnDestroy()
    {
        EventManager.OnPlayerDeath -= OnplayerDeath;
    }

    private void OnplayerDeath(Fighter obj)
    {
        Time.timeScale = 0;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
