using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    Transform player;
    public Vector3 Offset;
    void Start()
    {
        player = Fighter.instance.transform ;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = player.position+Offset;
    }
}
