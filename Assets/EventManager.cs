using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EventManager 
{
  
    public static event Action<AstroidPieces> OnAstroKilled;
    public static event Action<Fighter> OnPlayerDeath;
    public static event Action<Bullet,AstroidPieces> OnCollisionAstroBullet;
    public static event Action<Bullet> OnBulletShot;
    public static event Action<AstroidPieces> OnAstroSpawned;
    public static event Action<Fighter> OnPlayerSpawned;
    public static void PlayerSpawned(Fighter Fighter)
    {
        OnPlayerSpawned?.Invoke(Fighter);
    }
    public static void AstroKilled(AstroidPieces astroidPieces)
    {
        OnAstroKilled?.Invoke(astroidPieces);
    }
    public static void AstroSpawned(AstroidPieces astroidPieces)
    {
        OnAstroSpawned?.Invoke(astroidPieces);
    }
    public static void PlayerDeath(Fighter fighter)
    {
        OnPlayerDeath?.Invoke(fighter);
    }
    public static void CollisionAstroBullet(Bullet Bullet, AstroidPieces astroidPieces)
    {
        OnCollisionAstroBullet?.Invoke(Bullet,astroidPieces);
    }
    public static void BulletShot(Bullet Bullet)
    {
        OnBulletShot?.Invoke(Bullet);
    }

}
