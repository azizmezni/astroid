using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXManager : MonoBehaviour
{
    public GameObject astroKill;
    void Start()
    {
        EventManager.OnAstroKilled += onAstroKill;
    }
    private void OnDestroy()
    {
        EventManager.OnAstroKilled -= onAstroKill;
    }

    private void onAstroKill(AstroidPieces astroidPieces)
    {
        Instantiate(astroKill, astroidPieces.transform.position, astroidPieces.transform.rotation);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
